# MobileNetV3

---

模型名称：MobileNetV3

骨干网络：MobileNetV3

模块类型：cv-classification

可微调：True

输入形状： [224, 224, 3]

模型版本：1.0

作者：MindSpore团队

更新时间：2020-9-22

代码仓链接： <https://gitee.com/mindspore/models/tree/master/official/cv/mobilenetv3>

用户ID：MindSpore

用途：推理

训练后端：GPU

推理后端：GPU

MindSpore版本：1.0

许可证：Apache 2.0

摘要：使用MobileNetV3进行图片分类。

---

## 简介

该MindSpore Hub模型使用码云上MindSpore ModelZoo中的MobileNetV3实现，目录为model_zoo/official/cv/mobilenetv3。

更多详情参见[码云MindSpore ModelZoo](https://gitee.com/mindspore/models/blob/master/official/cv/mobilenetv3/Readme.md)。

## 参考论文

1. Howard, Andrew, Mark Sandler, Grace Chu, Liang-Chieh Chen, Bo Chen, Mingxing Tan, Weijun Wang et al. "Searching for mobilenetv3." In Proceedings of the IEEE International Conference on Computer Vision, pp. 1314-1324. 2019.

## Disclaimer

MindSpore ("we") do not own any ownership or intellectual property rights of the datasets, and the trained models are provided on an "as is" and "as available" basis. We make no representations or warranties of any kind of the datasets and trained models (collectively, “materials”) and will not be liable for any loss, damage, expense or cost arising from the materials. Please ensure that you have permission to use the dataset under the appropriate license for the dataset and in accordance with the terms of the relevant license agreement. The trained models provided are only for research and education purposes.

To Dataset Owners: If you do not wish to have a dataset included in MindSpore, or wish to update it in any way, we will remove or update the content at your request. Please contact us through GitHub or Gitee. Your understanding and contributions to the community are greatly appreciated.

MindSpore is available under the Apache 2.0 license, please see the LICENSE file.