# Resnet50_Thor

---

model-name: resnet50_thor

backbone-name: resnet50_thor

module-type: cv-classification

fine-tunable: True

input-shape: [224, 224, 3]

model-version: 1.5

accuracy: 0.759

author: MindSpore team

update-time: 2020-09-22

repo-link: <https://gitee.com/mindspore/models/tree/master/official/cv/resnet_thor>

user-id: MindSpore

used-for: inference

train-backend: ascend

infer-backend: ascend

mindspore-version: 1.0

license: Apache2.0

summary: resnet50_thor for image classification

---

## Introduction

This MindSpore Hub model uses the implementation of resnet50_thor from the MindSpore model zoo on Gitee at model_zoo/official/cv/resnet_thor.

More details please refer to the [MindSpore model zoo on Gitee](https://gitee.com/mindspore/models/blob/master/official/cv/resnet_thor/README.md).

## Citation

1. He K , Zhang X , Ren S , et al. Deep Residual Learning for Image Recognition[J]. 2016.

## Disclaimer

MindSpore ("we") do not own any ownership or intellectual property rights of the datasets, and the trained models are provided on an "as is" and "as available" basis. We make no representations or warranties of any kind of the datasets and trained models (collectively, “materials”) and will not be liable for any loss, damage, expense or cost arising from the materials. Please ensure that you have permission to use the dataset under the appropriate license for the dataset and in accordance with the terms of the relevant license agreement. The trained models provided are only for research and education purposes.

To Dataset Owners: If you do not wish to have a dataset included in MindSpore, or wish to update it in any way, we will remove or update the content at your request. Please contact us through GitHub or Gitee. Your understanding and contributions to the community are greatly appreciated.

MindSpore is available under the Apache 2.0 license, please see the LICENSE file.