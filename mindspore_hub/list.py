# Copyright 2020 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""
List networks or models from .

Loading network definition or pretrained model from mindspore mindspore_hub.
"""

import os
import shutil
import tempfile
from ._utils.download import _download_repo_from_url
from .manage import get_hub_dir


HUB_CONFIG_FILE = 'mindspore_hub_conf.py'
ENTRY_POINT = 'create_network'


def _delete_if_exist(path):
    """Delete backpu files"""
    if os.path.exists(path):
        if os.path.isdir(path):
            shutil.rmtree(path)
        else:
            os.remove(path)


def list(version=None, force_reload=True):
    r"""
    List all assets supported by MindSpore Hub.

    Args:
        version (str): Specify which version to list. None indicates the latest version. Default: None.
        force_reload (bool): Whether to reload the network from url. Default: True.

    Returns:
        list, a list of assets supported by MindSpore Hub.
    """
    if not isinstance(force_reload, bool):
        raise TypeError('`force_reload` must be a bool type.')

    repo_link = 'https://gitee.com/mindspore/hub.git'
    hub_dir = get_hub_dir()
    res_dir = os.path.join(hub_dir, 'mshub_res')

    if force_reload or (not os.path.isdir(res_dir)):
        if not force_reload:
            print(f'Warning. Can\'t find net cache, will reloading.')
        tmp_dir = tempfile.TemporaryDirectory(dir=hub_dir)
        _download_repo_from_url(repo_link, tmp_dir.name)
        _delete_if_exist(res_dir)
        os.rename(os.path.join(tmp_dir.name, 'hub.git', 'mshub_res'), res_dir)

    assets = []
    for device in os.listdir(os.path.join(res_dir, 'assets', 'mindspore')):
        for version in os.listdir(os.path.join(res_dir, 'assets', 'mindspore', device)):
            for md_file in os.listdir(os.path.join(res_dir, 'assets', 'mindspore', device, version)):
                asset = md_file[:-len('.md')]
                if asset not in assets:
                    assets.append(asset)

    assets.sort()
    return assets
